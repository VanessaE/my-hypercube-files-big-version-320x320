**BUILD LOG**

This project was started on 2020-05-18 and completed on 2020-10-08; the machine made its first good print on 2020-08-04.

The printer stands 540mm tall (plus the green/black feet and the enclosure top) and officially has a 320 x 320 x 320 mm build volume (in fact, slightly more, if you push it).

X uses 10 mm OD/8 mm ID carbon fibre tubes, Y uses 8 mm chromed steel rods, and Z uses 12 mm chromed steel rods.

I'm using Misumi Series-5 2020, 2040, and 4040 extrusions.

Newest entries in this log are at the bottom.

First, here is a screenshot, and a render of the machine without the enclosure panels (they would simply hide everything).  Colors of the printed parts are shown as intended, not necessarily what I will print them with (i.e. if I run out of some color). The aluminum extrusions depicted here were derived from Misumi's Series-5 technical drawings.

These images will be replaced from time to time as random bits of the project change or if I decide to screw around with my rendering settings :smile: ).

![](Images/example-screenshot.png)
![](Images/Render-direct.png)

The base machine:  https://www.thingiverse.com/thing:1752766

I'll be adding/using the following mods:

* This machine is designed with an integrated enclosure, which is why I went with a mixture of 2020, 2040, and 4040 -- the enclosure panels just slide into the extrusions' slots.  If not for the enclosure, it could have been made entirely of 2020.
* The piece of 2020 going across under the bed provides a place for the power supplies' middle-facing sides to attach to, and for the front of the bed to rest on when it's parked at the bottom of the print volume.
* A simple "pad" and two spacers (made by me) to attach to the crossbar and rear, lower X bar, so that the bed frame will sit level when parked, and won't make metal-on-metal contact on the crossbar.
* Stronger 8mm Y rod mounts: https://www.thingiverse.com/thing:2169166 
* XY motor angle adjusters: https://www.thingiverse.com/thing:2036245
* Bed mounting brackets:  https://www.thingiverse.com/thing:2471033 (plus some standoffs since I don't use bed springs)
* Corner brackets for the middle/back of the bed carriage:  https://www.thingiverse.com/thing:2740948 (modified by me to be much thicker)
* X carriage:  https://www.thingiverse.com/thing:2374167  (further modified by me to accept dual LM10UU bushings on top rather than an LM12LUU, and to alter the endstop mount position/angle)
* Z bearing mounts:  https://www.thingiverse.com/thing:2392973 (because I want to use regular LM12UU bearings, not long LM12LUU)
* The nameplate:  https://www.thingiverse.com/thing:2087379
* Mean Well SE-600 PSU model: https://grabcad.com/library/meanwell-se-600-48-power-supply-psu-1 (with minor modifications)
* PSU mounting bracket: https://www.thingiverse.com/thing:2467824 (but significantly reduced in size, plus a modified version to attach the right side of the PSU to the lower front frame piece)
* LM2596 buck converter: https://grabcad.com/library/lm2596-dc-dc-stepdown-converter-1
* Cable clips:  https://www.thingiverse.com/thing:2185517 (modified slightly, for size)
* Zip tie clip/mount:  https://www.thingiverse.com/thing:2863193 (modified slightly, for size)
* The pink LCD case at the top right and the green and white controller case at the rear, lower right are by me, and are derived from my generic BigTreeTech SKR v1.x/RRD 12864 case:  https://www.prusaprinters.org/prints/7686-case-enclosure-for-skr-and-rrd-12864-display
* The extruder is my Geared B'struder, https://www.prusaprinters.org/prints/7456-gregs-bwadestruder-geared-bstruder-bowden-extruder 
* The power switch mount is a customized version of the one I made for LACK table enclosures, https://www.prusaprinters.org/prints/7478-lack-table-power-switch-mount (incorporating a rocker switch for the mains, and a push-on-push-off switch for the lighting)
* The mains socket mount on the back of the machine is something I threw together just for this machine.
* Ditto for the spool holder.
* I've also added a mount for a 688ZZ bearing at the top of the lead screw, as a way of protecting it from lateral movement, since it's so long.
* Belt tensioner: https://www.thingiverse.com/thing:2396839 (modified by me to be as low-profile as possible)
* not shown in the render are printed slot covers I intend to add, and whatever bits I'll need to come up with for cable management.

# **2020-05-18**

Built the frame using the same end-tapping method as in my smaller machine.  Inches tape measure on top for scale (sorry, I don't have a proper metric one).  :stuck_out_tongue: 

![](Images/P1180605.JPG)

# **2020-05-19**

Installed the feet and lower enclosure panel.  In hindsight, I probably should have mounted it differently; I forgot to account for "overlap" of the corners of the panel with the 4040.  Little cuts in the corners fixed that, but it also meant taking the bottom-front of the frame apart to get the panel in.  The others will just slide in from the top (which is why the top back and sides are made from doubled-up 2020).

![](Images/P1180606.JPG)

# **2020-05-21**

Installed the Y and Z rods.

![](Images/P1180607.JPG)
![](Images/P1180609.JPG)

# **2020-05-22**

Installed the X gantry and carriage, Z bearing mounts and bed frame, and the front bedrest/crossbar (not pictured).  EDIT: I had to redo those bed mounting brackets due to a measuring error.

![](Images/P1180611.JPG)
![](Images/P1180612.JPG)

Also installed the XY motors, motor sag adjusters, and idler pulleys.

![](Images/P1180613-14.JPG)

# **2020-05-29**

Installed and equalized the belts, and installed some random bits and bobs including the bed rest pieces, enclosure door hinges and latch, 2020 end caps, and nameplate.

![](Images/P1180629.JPG)
![](Images/P1180633.JPG)
![](Images/P1180634-5.JPG)
![](Images/P1180642.JPG)

# **2020-06-12**

Installed the Z motor and lead screw, and squared-up them and the Z axis (I think).  Not shown:  also installed the upper bearing holder.

![](Images/P1180679.JPG)

# **2020-06-29**

I received the heated bed finally.  Damn, this thing's a lot bigger than I imagined.  :smile: I mean, I knew 328x328 wouldn't be small, but when you don't think about absolute size that much, and your other references are just on a computer screen (the frame doesn't count :wink:), well...

Unfortunately, it is NOT aluminum as the nomenclature claims -- the vendor sold me a counterfeit that's just a normal FR-4 (or similar) PCB.  Of course I filed a dispute, since I didn't get what I paid for.  I figure it'll probably still work, and it probably has the same hole pattern and dimensions as the genuine article, so I added LEDs and a resistor, wired it up, and used it as a guide to install and align the bed mounts.

As it turned out, I made some errors in my bed model, mainly the screw holes are closer together than I thought.  I took measurements from the actual heater and corrected the Blender model, but that left a fitting problem on the actual hardware... Rather than buy a shorter rear cross bar and then have to take the bed frame apart, reassemble it, and re-align the whole damned Z-axis, I simply reworked the bed mounts to move the screws a few millimeters closer.  I made slots instead of holes this time as well, so that there's some margin for error in case someone ends up with a heater with misaligned holes.

I also installed the bed frame center braces.

I will do the final install of the heater and its accouterments later, once I have something to connect the heater to.

![](Images/P1180698.JPG)
![](Images/P1180699.JPG)

# **2020-07-09**

Installed the power supply (Mean Well SE-600-24), power switches, and mains socket.  The little grey box with holes on the rear-facing end of the power supply has two female XT-30U connectors and holds an LM2596 buck converter set for 12 volts, to power the lighting.  I also made the cables and installed the LCD/enclosure module.

Although it's not really obvious from the photos, I did go ahead and install and test the lighting also.

![](Images/P1180718.JPG)
![](Images/P1180719.JPG)

# **2020-07-25**

My SKR v1.4 finally arrived, so I flashed a copy of Marlin (same as I use on my smaller Hypercube, but suitably-configured for this bigger machine), stuck the board in its enclosure for safety, and hooked up the 24v input, the heated bed, and the LCD.  Then mounted the bed with a thick layer of ceramic fibre insulation under it.  Put a 320x320 mm sheet of 3mm thick tempered glass on top, clamped it all together properly, and ran it through its paces: starting at room temp of 25°C, it gets to 80° in about 3 minutes, then 2 more minutes from there to 100°, and a total of about 10 minutes to get from 25° to 140°.

![](Images/P1180735.JPG)

# **2020-07-28**

I didn't like how the bed mount bracket was designed... when installed fully, the M3 screw blocks the side M5 screw, preventing me from mounting the bed onto the brackets and using it as the position/alignment reference for securing the brackets to the 2020, so I redesigned it.  I also added a third bed frame center brace, whose real purpose is actually just to keep the bed's insulation from dangling. :stuck_out_tongue:

Not seen in the photos, I took the time to square-up the frame and align the axes in every way I could think of.  

![](Images/P1180736.JPG)
![](Images/P1180737.JPG)

I also finalized most of the wiring.  I think I did a better job this go-round, compared to previous machines.

![](Images/P1180740.JPG)

Also, that ceramic fibre bed insulation is as horrible to handle as fibreglass - so irritating and itchy.  But more importantly, since I had to "peel" the insulation apart to halve its thickness, the edges and one side are fragile and prone to shedding/tearing.  So that it won't get everywhere, I sewed it inside a "bag" made from an old pillowcase. Seems to contain it pretty well.

One problem though... with it inside that bag, the insulation's still a little too thick, and presses up on the center of the bed too much, enough to bend the *glass*.  I decided that I'd rather sacrifice a tiny amount of Z travel than to try to crush the insulation down, so I put 10 mm spacers under the bed, in lieu of the 4 mm ones I had before.  Since the 4 mm would still be just fine if my insulation were thinner (as I originally had planned on just putting in a sheet of cardboard when I designed the machine), I'm still gonna consider the machine as having officially 300 mm effective Z travel.

![](Images/P1180742.JPG)

# **2020-08-02**

My OCD :smile: demanded I resolve the loss of Z caused by those taller bed spacers, so I made some modifications elsewhere to increase the machine's Z travel.

Most of the increase came from simply lowering the lower X/Y frame pieces a bit.  But, in order to keep my existing Z rods and screw, I had to redesign the Z rod mount brackets (now just one shape for all four points), the lead screw top bearing mount, mains socket housing and its cover, front power switch mount, and the left, right, back, bottom, and door of the enclosure.  Since these changes lowered the Z motor and PSU a lot, I also altered the feet (TPU "ball" part only) to raise the whole machine up to make room.

I also redesigned the leg bottom drill jig as well.  It now has a 33 mm hole offset (from the end of the 4040), and has "wings" to allow it to also serve as a positioning and alignment jig for the X/Y frame pieces that the drill holes are for.

These changes increased Z to over 320 millimeters of travel with the 10 mm bed spacers in place.  Making those spacers the go-to size gives the machine a full 320x320x320 mm build volume, i.e. a proper cube!  (though my particular build still has the too-short bed frame, so I'm still short a couple millimeters along Y).

This also has the pleasing side effect of pulling the tops of the Z rods and lead screw, and their respective upper mounts, back inside the frame.

![](Images/P1180745-6.JPG)

# **2020-08-05**

I finally got the fans, PTFE, and T-nuts I was waiting on (after giving the vendors from China WAY more leeway than they should have needed, and just re-ordering from Amazon to speed things along), and was able to finally put the last bits onto the printer, test the extruder, and apply the Printbite to the bed.  I already have what I need for the enclosure, and will do that part later.  

The Benchy seen on the bed (though not in focus) was printed with this machine (then removed, examined, and placed back on the bed for the photo), and came out pretty good.  Nothing wrong with it that can't be traced back to slicer bugs.  Quality is comparable to my smaller Hypercube.

Technically this finishes the actual printer, save for the enclosure and decorative stuff like T-slot covers (I out of bright green, so I'll need to wait on those anyway), but I'm not yet convinced about the behavior of the bed, as it's cantilevered like most bed-moves-in-Z machines, and vibrates when it's suddenly moved up/down (especially the first approach when homing, because of that instantaneous stop when the probe triggers).  I may rework things to put rods and screws at both ends of the bed (with a single Z motor driving both sides with a belt), but I'll first need to run more tests.  PLus that'll mean buying more parts. :stuck_out_tongue:

The bed and Z parts are already plenty strong, and while I haven't perfectly leveled it yet, I was able to resolve the cantilever-related sag more than adequately.  There's no appreciable play/slop in the mechanical parts, just "flex" because of the sheer size of the bed, so if I could find some way to dampen the vibrations, that would be enough.  Maybe add some weight to the front of the bed?

![](Images/P1180748.JPG)

# **2020-08-06**

I had to swap the cold end fan for a 5015 blower because the RPW-Ultra requires a ton of cooling to prevent heat creep.

Though I was hoping to get by with the 40x10 fan since it's quieter, I expected I'd have to do this, as using a 40x10 fan didn't work when I had my Ultra in my old Prusa i3, which was also enclosed.  A 5015 blower keeps the hottest part of the heat sink plenty cool (about 55°C, when the hotend is at 240°, according to my digital thermometer).  Even PETG was jamming-up with the 40x10, meaning that the heat sink was exceeding 80°.  Now if I could just get the extruder working right (it gave up the ghost right after printing that Benchy and hasn't worked since).

![](Images/P1180749.JPG)

# **2020-08-21**

Finally got the hobbed gear I was waiting on, and after some minor extruder revisions (which brought with it the need to simply print new parts), the machine is able to print reliably.  Now the fun part: fine tuning.

![](Images/P1180767.JPG)

# **2020-10-08**

The machine's all tuned-up now, and I was finally able to spring for the acrylic and opaque white panels I needed to finish the final version of the enclosure.  This completes the project.

![](Images/P1180798.JPG)

# **2021-09-24**

I've been tinkering around with the design, trying to make parts stronger where needed, reduce mass where practical, and also created a direct-feed geared toolhead/extruder using a "pancake" NEMA17 stepper (6.538:1 printed gearing).

![](Images/P1180965.JPG)

# **2021-10-30**

I continued to tinker, just couldn't help it.  One thing that I realized once I got that direct toolhead going was that I needed to stiffen-up the XY joiners to better-handle the increased mass, as the upper/lower tube clamps in the original XY joiner design had a tendency to twist around local Z, allowing the tubes to flex too much.  The toolhead's center of gravity is a bit high, so it wants to twist around the X axis on sharp Y moves, resulting in excessive "nodding", manifesting as ringing in prints.  To further stiffen-up the system, I also switched to 10mm OD, 6mm ID tubes, which are 33% more rigid than the standard 10/8 mm tubes, with barely any increase in mass.

![](Images/P1190006.JPG)

I also went through a few revisions of the toolhead, tried using a 40 mm fan for a while to cool the cold end's heat sink, then went back to a 5015 due to heat creep.  The design I settled on isn't too different from what I originally had in September, but it's maybe a bit better-balanced now, and hopefully a bit more rigid and lower mass also.  The upper X bushing clamp also has a guard on it to keep the toolhead's wires from potentially snagging on the upper Z rod and screw mounts on back.

![](Images/P1190003.JPG)

The *real* work though was altering the Z setup to use three lead screws to support and move the bed, via one motor and a 1.35m x 6mm GT2 belt loop, eliminating the cantilever aspect, as that design allowed the front of the bed to vibrate up and down on every tiny little Z move (actually, even X/Y moves would induce vibrations).  Sure, the extra parts slow the upwards Z travel speed a little bit, but it's worth it, and it still moves pretty quick anyway (75 mm/s when homing or otherwise going up, 200 mm/s when going downwards during normal layer moves or when parking the bed).

What's not clearly shown in the photo is that each front lead screw has a 688ZZ bearing at each end inside its printed mount, and the bottom end of each screw has a printed spacer, the GT2 pulley, and a steel locking collar (with grub screw for extra holding power), all pressed firmly together against the lower bearing.  This lower stack thus supports the weight of the front of the bed on the bearing.  The upper end of each lead screw has a printed locking collar pressed firmly against that end's bearing as well.  Together, these parts keep the screws from shifting up and down or touching the frame on either end.  I figure the mass of the bed, its frame, and a hypothetical ginormous print sitting on it won't add up to more than a few kilograms, and each bearing only has to tolerate about a third of that total mass anyway (so maybe 1 kilogram per screw).  I'm pretty sure that's well within the axial load limit of the bearings. :-)

The initial mechanical part of bed leveling is then done by locking the motor (i.e. by doing say `M84 S9999` followed by a short Z jog), then loosening one or both of the front pulleys' grub screws while keeping the locking collars fixed to the lead screws to keep them vertically-stable, and turning each screw relative to the pulley to move that corner of the bed up or down.

![](Images/P1190002.JPG)

# **2022-0-4-24**

I'd been doing a number of small upgrades and changes here and there, most notably moving the spool holder inside the machine, and adapting its motherboard case to include an external bed MOSFET, and I built a decorative frame for the top section of the enclosure, inspired by something I saw on a Facebook post.  Here's how the machine looks now:

![](Images/P1030611.JPG)
