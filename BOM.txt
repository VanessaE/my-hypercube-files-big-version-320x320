BOM: 540x510x540 Hypercube, 320x320x320 build volume
----------------------------------------------------

4040:

* 4, 540 mm

2040:

* 3, 460 mm
* 2, 430 mm

2020:

* 1, 185 mm
* 2, 256 mm
* 4, 430 mm
* 2, 460 mm
* 2, 470 mm

AXES:

X:

* 2, 10 mm outer diameter carbon fibre roll-wrapped tubes, 480-500 mm 
  long, 6mm ID preferred, if using the direct-feed toolhead (otherwise 
  8mm ID is fine)
* 3, LM10UU drylin bushing (better would be something the same 
  dimensions as these, but with a spiral flute inside instead of straight 
  ribs)

Y:

* 2, 8 mm diameter linear rods, 400-430 mm long
* 2, LM8LUU (long) bearings

Z:

* 2, 12 mm diameter linear rods, 500-570 mm long
* 4, LM12UU bearings, or 2 LM12LUU (long) bearings
* 3, TR8x8 leadscrew, 500 mm long, with couplers/nuts -- cut two of them to 470-475 mm [1]
* 3, 20-tooth 8mm bore GT2 pulleys with grub screws [1]
* 1, 1350 mm GT2 belt loop (675 teeth) [1]
* 1, 688ZZ bearings
* 4, 688ZZ bearings (additional, total of 5) [1]


ENCLOSURE (ideally all acyrlic unless specified):

Lower:
* 2, 441 x 490 (sides)
* 1, 471 x 490 (back)
* 1, 471 x 431 (bottom) [2]
* 1, 382 x  86 (front, top portion)
* 1, 458 x 394 (door)

Upper:
* 2, 476 x 200 (front, back)
* 2, 440 x 200 (sides)
* 1, 476 x 446 (top)

MISC:
2, PC4-M6 pneumatic fittings (preferably the all-the-way-through kind)
** 4mm OD/2 mm ID PTFE tubing, about 75 cm

4, NEMA17 stepper motors, make 1 a pancake if using the direct-feed toolhead
2, 20-tooth 5mm bore GT2 pulleys (or 8mm if that's what your X/Y motors use)
16, F623ZZ flange bearings
8, F623ZZ flange bearings (additional, total of 24) [1]
* 5m GT2 2x6mm belt

1, Genuine J-head 11 or RPW-Ultra hotend
1, BLtouch, 3DTouch, or other clone
1, 5015 blower fan
1, 40x10 mm PC fan
2, 50x20 mm PC fans

1, Mean Well SE-600-24 power supply
1, LM2596 DC-DC buck converter
* Lighting of some kind (I used 12v LED bars meant for car/van interior)
* rocker and push-on-push-off switches
* 14-16 gauge DC power wire, about 2m

1, RepRapDiscount Full-Graphic 12864 display module
1, SKR v1.1, 1.3, 1.4, or v2
4, TMC2208 driver modules (or whatever you prefer)

2, standard Makerbot-style endstop switch modules
* extra long cables and connectors for LCD
* 2, 3, 4 pin JST-XH pigtails
* lots of wire, assorted colors
* mesh sleeving, assorted sizes
* M5 tap

* 1, MK3a heated bed for "300x300" print area (they're actually about 
  328x328 mm)
* 1, 320x320 glass plate
* some kind of suitable insulation for under the bed, I used a ceramic 
  fibre blanket.

* Assorted M5 screws, 8 to 16 mm
* assorted M3 screws
* a bunch of M3 and M5 drop-in T-nuts

[1] Only needed if you plan to put leadscrews on the front.  They're optional;
    the machine will work fine with just the back one, but the front of the
    bed will vibrate significantly on Z moves, and may wobble side to side,
    due to its sheer size.

[2] this piece has multiple, custom cuts, and can be any rigid material,
    opaque or transparent, and not necessarily acrylic, since one might want to
    obscure the power supply et al. under the machine.



